import React from "react";

const Header = () => {
  return (
    <div
      className=""
      style={{
        width: "100%",
        height: "100px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(44, 40, 38, 0.71)",
        position: "fixed",
      }}
    >
      <p className="text-white text-center">TRY GLASSES APP ONLINE</p>
    </div>
  );
};

export default Header;
