import React, { useState } from "react";
import data from "./dataGlasses.json";
import ListGlasses from "./ListGlasses";
import GlassesChange from "./GlassesChange";
const Body = () => {
  const [changeGlass, setChangeGlass] = useState(data[0]);
  console.log("changeGlass :", changeGlass);
  const handleChangeGlass = (glass) => {
    setChangeGlass(glass);
  };
  return (
    <div
      style={{
        backgroundImage: "url(./glassesImage/background.jpg)",
        width: "100%",
        height: "100vh",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="model" style={{ position: "relative" }}>
        <div className="model-top">
          <GlassesChange changeGlass={changeGlass} />
          <div className="model-right">
            <img
              style={{
                position: "absolute",
                top: "200px",
                right: "300px",
                width: "200px",
                height: "220px",
              }}
              src="./glassesImage/model.jpg"
              alt=""
            />
          </div>
        </div>
      </div>
      <div
        className="model-bottom row"
        style={{
          position: "absolute",
          top: "60%",
          left: "19%",
          width: "970px",
          height: "250px",
          backgroundColor: "white",
          boxShadow:
            "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px",
          zIndex: "2",
        }}
      >
        {data.map((glass) => {
          return (
            <ListGlasses
              key={glass.id}
              glass={glass}
              handleChangeGlass={handleChangeGlass}
            />
          );
        })}
      </div>
    </div>
  );
};

export default Body;
