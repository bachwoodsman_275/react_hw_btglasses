import React from "react";

const ListGlasses = (props) => {
  const { glass, handleChangeGlass } = props;
  return (
    <div
      className="col-2 mt-4"
      style={{
        cursor: "pointer",
      }}
      onClick={() => handleChangeGlass(glass.url)}
    >
      <img className="w-100" src={glass.url} alt="..." />
    </div>
  );
};

export default ListGlasses;
