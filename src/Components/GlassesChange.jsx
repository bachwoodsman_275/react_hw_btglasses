import React from "react";
const GlassesChange = ({ changeGlass }) => {
  return (
    <div
      className="model-left d-flex flex-column align-items-center"
      style={{
        position: "relative",
        top: "200px",
        left: "300px",
        width: "200px",
        height: "220px",
        background: "url(./glassesImage/model.jpg)",
        backgroundSize: "100%",
      }}
    >
      <img
        style={{
          position: "relative",
          top: "50px ",
          width: "100px",
          height: "45px",
        }}
        src={changeGlass}
        alt=""
      />
    </div>
  );
};

export default GlassesChange;
