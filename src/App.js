import logo from "./logo.svg";
import "./App.css";
import Header from "./Components/Header";
import Body from "./Components/Body";

function App() {
  return (
    <div className="App " style={{ position: "relative" }}>
      <Header />
      <Body />
    </div>
  );
}

export default App;
